<?php

namespace Bognerf\Purify\Tests;

class PurifyTest extends TestCase
{
    public const XSS_REGEX_TEST = '/(onclick|alert|onmouseover|onload|script|javascript\:)/i';

    public function testFacadeWorks(): void
    {
        self::assertInstanceOf(\HTMLPurifier::class, \Purify::getInstance());
    }

    public function someXssAttacksProvider(): array
    {
        return [
            'onclick_insertion' =>
                ['<a href="https://florianbogner.de" onclick="alert(1)">Klick me, fool!</a>'],
            'script_generic' =>
                ['<script type="text/javascript">alert(1)</script>'],
            'img_src' =>
                ['<IMG SRC="javascript:alert(1)"/>'],
        ];
    }

    /**
     * @param $xssAttack
     * @dataProvider someXssAttacksProvider
     */
    public function testXssIsRemoved($xssAttack): void
    {
        self::assertEquals(1, preg_match(self::XSS_REGEX_TEST, $xssAttack));
        self::assertEquals(0, preg_match(self::XSS_REGEX_TEST, \Purify::purify($xssAttack)));
    }
}
