<?php

namespace Bognerf\Purify\Tests;

use Bognerf\Purify\PurifyFacade;
use Orchestra\Testbench\TestCase as Orchestra;
use Bognerf\Purify\PurifyServiceProvider;

class TestCase extends Orchestra
{


    protected function getPackageProviders($app)
    {
        return [
            PurifyServiceProvider::class,
        ];
    }

    protected function getPackageAliases($app)
    {
        return [
            'Purify' => PurifyFacade::class,
        ];
    }

    public function getEnvironmentSetUp($app)
    {
        $app['config']->set('purify.encoding', 'UTF-8');

    }
}
