<?php

return [
    'cache_dir' => env('PURIFY_CACHE_DIR', storage_path('framework/purify')),
    'encoding' => env('PURIFY_ENCODING', 'UTF-8'),
];
