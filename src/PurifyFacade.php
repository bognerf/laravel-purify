<?php

namespace Bognerf\Purify;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Bognerf\Purify\Purify
 */
class PurifyFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'Purify';
    }
}
