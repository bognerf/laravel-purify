<?php

namespace Bognerf\Purify;

use Illuminate\Support\ServiceProvider;

class PurifyServiceProvider extends ServiceProvider
{

    public function register()
    {
        $this->app->singleton('Purify', function ($app) {
            $config = \HTMLPurifier_Config::createDefault();
            $config->set('Cache.SerializerPath', config('purify.cache_dir'));
            $config->set('Core.Encoding', config('purify.encoding'));
            return new \HTMLPurifier($config);
        });
    }

    public function boot()
    {
        // Publish config file
        $this->publishes([
            __DIR__ . '/../config/purify.php' => config_path('purify.php'),
        ], 'config');

        // Publish storage path containing .gitignore file
        $this->publishes([
            __DIR__ . '/../storage/framework/purify' => storage_path('framework/purify')
        ], 'HTMLPurify Cache Directory');
    }
}
