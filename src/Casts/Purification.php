<?php


namespace Bognerf\Purify\Casts;


use Illuminate\Contracts\Database\Eloquent\CastsAttributes;

class Purification implements CastsAttributes
{

    public function get($model, string $key, $value, array $attributes)
    {
        return $value;
    }

    public function set($model, string $key, $value, array $attributes)
    {
        return \Purify::purify($value);
    }
}