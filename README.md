# Laravel Package to inlcude HTMLPurify

[![coverage report](https://gitlab.com/bognerf/laravel-purify/badges/master/coverage.svg)](https://gitlab.com/bognerf/laravel-purify/-/commits/master)
[![Latest Version on Packagist](https://img.shields.io/packagist/v/bognerf/laravel-purify.svg?style=flat-square)](https://packagist.org/packages/bognerf/laravel-purify)
[![Total Downloads](https://img.shields.io/packagist/dt/bognerf/laravel-purify.svg?style=flat-square)](https://packagist.org/packages/bognerf/laravel-purify)

`laravel-purifier` includes the famous [HTMLPurifier](http://htmlpurifier.org/) library as provided by
[ezyang/htmlpurifier](https://github.com/ezyang/htmlpurifier). As it - of course - makes use of Laravel's convenient
package auto-discovery features, all you have to do is require this library once and publish its assets.

Afterwards, you can simply use HTMLPurifier via Facade:

```php
$sanitizedString = \Purify::purify($potentiallyMaliciousString);
```

Please, keep in mind that
* disabling HTMLPurifier's cache is not recommended
* HTMLPurifier::purify() or as of this package \Purify::purify() does some intensive parsing and should only be used
  where necessary. You don't have to purify strings which are already escaped, like they are in Laravel Blade when
  included via `` {{ $string }} ``. But you should definitely think about using \Purify::purify() on strings included
  via `` {!! $string !!} ``, resulting in `{!! \Purify::purify($string) !!}`.

## Installation

### Composer

```shell script
$ composer require bognerf/laravel-purify
```


### Publish

```shell script
$ php artisan vendor:publish --provider="Bognerf\Purify\PurifyServiceProvider"
```

This will copy the config file `purify.php` to your Laravel's config directory, and it also will create a folder
`purify` under Laravel's storage/framework directory for some cache and serializer files of [HTMLPurifier](http://htmlpurifier.org/) .


### Configuration

The way HTMLPurifier is configured [out-of-the-box](http://htmlpurifier.org/docs#toclink0) in this library, it should
work robustly against XSS attacks and further malicious strings in your HTML and URLs. So there is nothing you could
configure to change its behaviour, currently.

#### Cache directory

By default, the cache directory for HTMLPurifier is set to `LARAVEL_ROOT/storage/framework/purify`. You may change it in
`config/purify.php` or via .env as `PURIFY_CACHE_DIR`. Make sure the directory is writeable for your webserver user or
group.

In case you want to disable caching for whatever reason, set `PURIFY_CACHE_DIR` to `null`.

#### Encoding

Well, we shouldn't use anything different from `UTF-8` these days and changing this parameter is somewhat discouraged,
but you could do it in `config/purify.php` or via .env as `PURIFY_ENCODING`.




